15.  (SOAL ESSAY E)	
	SELECT id,
	SUM(IF( customer_id = 1, amount, 0)) AS total_amount,
        SUM IF( customer_id = 2, amount, 0)) AS total_amount
     FROM orders
     LEFT JOIN customer_name USING(customer_id) 

16. (SOAL ESSAY C)

    create table customers (
    -> id int primary key auto_increment,
    -> name varchar(255),
    -> email varchar(255),
    -> password varchar(255)
    -> );
   
     create table orders (
    -> id int primary key auto_increment,
    -> amount varchar(255),
    -> customer_id int
       );

     ALTER TABLE orders
     ADD FOREIGN KEY (customer_id) REFERENCES customers(id);

17. (SOAL ESSAY D)
   
   insert into customers values
   ('', 'John Doe', 'john@doe.com',  'john123'),
   ('', 'Jane Doe', 'jane@doe.com',  'jenita123');

   insert into orders values
   ('', '500', '1'),
   ('', '200', '2'),
   ('', '750', '2'),
   ('', '250', '1'),
   ('', '400', '2');

